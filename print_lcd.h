#ifndef PRINT_LCD_H
#define PRINT_LCD_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>

#include "mzapo_parlcd.h"

#define LCD_W 320
#define LCD_H 480
#define LET_W 8
#define LET_H 16
#define WHITE 0xffff
#define SIZE_LET 7
void clear_lcd(unsigned char* base);

void write_lcd(unsigned char* base, bool* array);

void bitmap_to_bin(void);

void read_bitmap(int letter);

void square_lcd(int shift, int zoom, bool* array);

void zoom_let(int x, int y, int letter,int zoom, bool* array);

#endif
