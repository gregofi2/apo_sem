#include "print_lcd.h"
#include "font_rom8x16.c"

int hex_m[16];
int bin_m[16][10];

//clears the display
void clear_lcd(unsigned char* base){
	for(int i = 0; i < LCD_W * LCD_H; i++)
		parlcd_write_data(base, 0);
}

//writes the array to the display
void write_lcd(unsigned char* base, bool* array){
	unsigned c;
	for(int i = 0; i < LCD_W; i++){
		for (int j = 0; j < LCD_H; j++){
			if(array[i + j*LCD_W]) c = WHITE;
			else c = 0x0;
			parlcd_write_data(base, c);
		}
	}
}

//converts a hexadecimal bitmap to bits
void bitmap_to_bin(void){
	for(int i = 0; i < LET_H; ++i)
		for(int j = 0; j < LET_W; ++j)
			bin_m[i][j] = (hex_m[i]>>j) & 1;
}

//loads a bitmap
void read_bitmap(int letter){
	int shift =  letter * LET_H;
	for(int i = 0; i < LET_H; ++i)
		hex_m[i] = rom8x16_bits[shift + i]>>8;
}

//in the array sets the values to a square shape
void square_lcd(int shift, int zoom, bool* array){
	for (int i = 0; i < zoom; i++){
		for (int j = 0; j < zoom; j++)
			array[shift + j + i * LCD_W] = 1;
	}
}

//enlarges the loaded character
void zoom_let(int x, int y, int letter,int zoom, bool* array){
	read_bitmap(letter);
	bitmap_to_bin();
	int x_sh = x * zoom * LET_W;
	int y_sh = y * zoom * LET_H;
	for (int i = 0; i < LET_H * zoom; i+=zoom){
		for (int j = LET_W*zoom; j > 0;j-=zoom){
			if(bin_m[i/zoom][j/zoom]){
				int shift = y_sh + i + (x_sh + (LET_W * zoom - j)) * LCD_W;
				square_lcd(shift, zoom, array);
			}
		}
	}
}


