#include "leds_control.h"

//lights up one more LED than it does now
void power_up_disp(int* counter, unsigned char* mem){
	uint32_t led = 0;
	for (int i = 0; i <= *counter; i++) led += pow(2, MAX_LEDS - 1 - i);
	*(volatile uint32_t*)(mem + SPILED_REG_LED_LINE_o) = led;
	*counter += 1;
}
//goes out one LED more than it is now
void power_down_disp(int* counter, unsigned char* mem){
	uint32_t led = 0;
	for (int i = 0; i < (*counter - 1); i++) led += pow(2, MAX_LEDS - 1 - i);
	*(volatile uint32_t*)(mem + SPILED_REG_LED_LINE_o) = led;
	*counter -= 1;
}

int set_led_line(unsigned char* base, int light_leds){
	uint32_t set = 0;
	if(light_leds > MAX_LEDS) light_leds = MAX_LEDS;
	for(int i = 0; i < light_leds; i++) set += pow(2, MAX_LEDS -1 -i);
	*(volatile uint32_t*)(base + SPILED_REG_LED_LINE_o) = 0;
	*(volatile uint32_t*)(base + SPILED_REG_LED_LINE_o) = set;
	return light_leds;
}

//all LEDs go out
void all_leds_off(int* counter, unsigned char* mem){
	*(volatile uint32_t*)(mem + SPILED_REG_LED_LINE_o) = 0;
	*counter = 0;
	*(volatile uint32_t*)(mem + SPILED_REG_LED_RGB1_o) = 0;
	*(volatile uint32_t*)(mem + SPILED_REG_LED_RGB2_o) = 0;
}
//lights up RGB LEDS
void RGB_color(unsigned char* mem, char color, int led_num){
	switch(color){
		case 'r':
			if (led_num == FIRST_LED || led_num == BOTH_LEDS)
				*(volatile uint32_t*)(mem + SPILED_REG_LED_RGB1_o) = RED;

			if (led_num == SECOND_LED || led_num == BOTH_LEDS)
				*(volatile uint32_t*)(mem + SPILED_REG_LED_RGB2_o) = RED;
			break;
		case 'g':
			if (led_num == FIRST_LED || led_num == BOTH_LEDS)
                                *(volatile uint32_t*)(mem + SPILED_REG_LED_RGB1_o) = GREEN;

                        if (led_num == SECOND_LED || led_num == BOTH_LEDS)
                                *(volatile uint32_t*)(mem + SPILED_REG_LED_RGB2_o) = GREEN;
			break;

		case 'y':
			if (led_num == FIRST_LED || led_num == BOTH_LEDS)
                                *(volatile uint32_t*)(mem + SPILED_REG_LED_RGB1_o) = YELLOW;

                        if (led_num == SECOND_LED || led_num == BOTH_LEDS)
                                *(volatile uint32_t*)(mem + SPILED_REG_LED_RGB2_o) = YELLOW;
			break;
	}
}
