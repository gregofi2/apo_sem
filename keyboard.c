#include "keyboard.h"

void set_dir(data_t* data){
	printf("choose a direction\n"); 
	printf("'r' for clockwise or 'l' for counterclockwise\n");
	int c = getchar();
	switch(c){
		case 'r':
			data->dir = DCSPDRV_REG_DUTY_DIR_A_m;
			break;
		case 'l':
			data->dir = DCSPDRV_REG_DUTY_DIR_B_m;
			break;
		default:
			printf("wrong char, nothing changed\n");
			break;
	}
}

void set_rotation(data_t* data){
	call_termios(1);
	int number;
	char c;
	printf("Set rotation per second in interwal between 5-50\n");
	while(1){
		if(scanf("%d",&number) != 1){
			c = getchar();
			printf("'%c' is wrong input\n", c);
		}
		else{
			if(MIN_ROT <= number && number <= MAX_ROT){
				printf("Rotation set: %d\n",number);
				data->rotation_user = number;
				break;
			}
			else printf("Rotation sert ot of range\n");
		}
		printf("Try again\n");
	}
	call_termios(0);
	c = getchar();
}

void init_rotation(data_t* data){
	*(volatile uint32_t*)(data->motor_base + DCSPDRV_REG_CR_o) = 0;
	*(volatile uint32_t*)(data->motor_base + DCSPDRV_REG_PERIOD_o) = 0;
	*(volatile uint32_t*)(data->motor_base + DCSPDRV_REG_PERIOD_o) = 5000;
	*(volatile uint32_t*)(data->motor_base + DCSPDRV_REG_CR_o) = DCSPDRV_REG_CR_PWM_ENABLE_m;
	*(volatile uint32_t*)(data->motor_base + DCSPDRV_REG_DUTY_o) =  data->pwm * 5 | data->dir;
}

void call_termios(int reset){
	static struct termios tio, tioOld;
	tcgetattr(STDIN_FILENO, &tio);
	if (reset) {
		tcsetattr(STDIN_FILENO, TCSANOW, &tioOld);
	}
	else {tioOld = tio; //backup
		cfmakeraw(&tio);
		tio.c_oflag |= OPOST;
		tcsetattr(STDIN_FILENO, TCSANOW, &tio);
	}
}


void* keyboard_th(void*d){
	printf("keyboard_th start\n");
	data_t *data = (data_t*)d;
	static int ret = 0;
	int c;
	bool quit = false;
	call_termios(0); // store terminal settings
	while(!quit){
		c = getchar();
		pthread_mutex_lock(&mtx);
        	switch(c){
			case 'q':
				data->quit = true;
			case 's':
				//stop motor
				printf("motor stop\n");
				data->pwm = 0;
				data->rotation_user = 0;
			        init_rotation(data);
				break;
	                case 'i':
				//set rotation
				set_rotation(data);
				break;
			case 'o':
				if(data->pwm != 0)
					printf("Motor is running can not change direction\n");
				else set_dir(data);
				break;
			case 'b':
				printf("regulated stop\n");
				data->rotation_user = 0;
				RGB_color(data->leds_base, 'y', BOTH_LEDS);
				break;
			case 'a':
				//start motor
				printf("motor start\n");
				if(data->rotation_user != 0){
					RGB_color(data->leds_base, 'r', BOTH_LEDS);
					data->pwm = DEF_PWM;
				        init_rotation(data);
			        }
				else printf("Rotation per set is 0\n");
				break;
			default:
				printf("'%c' is wrong input\n", c);
				break;
		}
	       	pthread_mutex_unlock(&mtx);
		quit = is_quit(data);
	}
    	printf("set quit\n");
    	pthread_mutex_lock(&mtx);
    	//unlock convard and mutex
    	pthread_cond_broadcast(&conv);
    	pthread_mutex_unlock(&mtx);
    	call_termios(1); // restore terminal settings
    	printf("keyboard_th end\n");
    	return &ret;
}
