#ifndef LEDS_CONTROL_H
#define LEDS_CONTROL_H

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h> 
#include <math.h>

#include "mzapo_regs.h"

#define MAX_LEDS 32
#define RED 0XFF0000
#define YELLOW 0XFFFF00
#define GREEN 0XFF00
#define FIRST_LED 1
#define SECOND_LED 2
#define BOTH_LEDS 3    

void power_up_disp(int* counter, unsigned char* mem);

void power_down_disp(int* counter, unsigned char* mem);

int set_led_line(unsigned char* base, int light_leds);

void all_leds_off(int* counter, unsigned char* mem);

void RGB_color(unsigned char* mem, char color, int led_num);

#endif
