#ifndef MAIN_H
#define MAIN_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>

#include <termios.h>
#include <unistd.h>  // for STDIN_FILENO

#include <pthread.h>

#include "mzapo_phys.h"
#include "mzapo_regs.h"
#include "leds_control.h"
#include "print_lcd.h"

#define DELAY_S 1
#define TENTH_BIT 1024
#define MIN_ROT 5
#define MAX_ROT 50
#define IRQ_PUL 128
#define PWM_STEP 5
#define LINES 2
#define MAX_PWM 100
#define DEF_PWM 35
#define LEN_STR 20

typedef struct data{
        bool quit;
        int leds_counter;
        int turns_counter;
        int rotation;
        int lcd_rotation;
	    unsigned char * motor_base;
	    unsigned char* leds_base;
        
        int pwm;
        int rotation_real;
        int rotation_user;
        uint32_t dir;
        
}data_t;

pthread_mutex_t mtx;
pthread_cond_t conv;

void* keyboard_th(void*);
void* motor_th(void*);
void* display_th(void*);
void* period_th(void*);


bool is_quit(data_t* data);

void clear_array(bool* array);

#endif
