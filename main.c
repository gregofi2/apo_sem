/*******************************************************************
  Project main function for MicroZed based MZ_APO board
  designed by Libor Dubský and Filip Gregor

  main_threads.c      - main file
 *******************************************************************/


#include "main.h"


bool is_quit(data_t* data){
	pthread_mutex_lock(&mtx);
	bool q = data->quit;
	pthread_mutex_unlock(&mtx);
	return q;
}
void clear_array(bool* array){
	for(int i = 0; i < LCD_W * LCD_H; i++) array[i] = false;
}


int main(int argc, char *argv[]){
	data_t data = {.quit = false,
			.leds_counter = 0,
			.turns_counter = 0,
			.rotation = -1,
	        	.pwm = 0,
			.motor_base = map_phys_address(DCSPDRV_REG_BASE_PHYS_0, DCSPDRV_REG_SIZE , 0),
			.leds_base = map_phys_address(SPILED_REG_BASE_PHYS, SPILED_REG_SIZE,0),
	        	.rotation_real = 0,
			.lcd_rotation =  0,
			.rotation_user = 0,
			.dir = DCSPDRV_REG_DUTY_DIR_A_m
	};

	if(data.motor_base == NULL || data.leds_base == NULL){
		printf("base failed. End program\n");
		exit(2);
	}
	all_leds_off(&data.leds_counter, data.leds_base);
	enum {KEYBOARD, MAIN, DISPLAY, PERIOD, NUM_THREADS};
	const char* threads_names[] = {"Keyboard", "Motor", "Display", "Period"};
    	void* (*thr_functions[])(void*) = {keyboard_th, motor_th, display_th, period_th};
    	//array of threads
	pthread_t threads[NUM_THREADS];
    	//init mutex and convard
   	pthread_mutex_init(&mtx, NULL);
    	pthread_cond_init(&conv, NULL);
 
    	//create threads
    	for(int i = 0; i < NUM_THREADS; i++){
        	int r = pthread_create(&threads[i], NULL, thr_functions[i], &data);
        	printf("create thread '%s' %s\r\n", threads_names[i], (r == 0 ? "OK" : "FAIL"));
    	}
   	int *ex;
    	for(int i = 0; i < NUM_THREADS; i++){
        	printf("Call join to the thread %s\r\n", threads_names[i]);
        	int r = pthread_join(threads[i], (void*)&ex);
        	printf("Joining the thread %s has been %s\r\n", threads_names[i], (r == 0 ? "OK" : "FAIL"));
    	}
	all_leds_off(&data.leds_counter, data.leds_base);
    	return EXIT_SUCCESS;
}


void* display_th(void*d){
	printf("display_th start\n");
	data_t *data = (data_t*) d;
	bool quit = false;
	static int ret = 0;
	unsigned char * parlcd_mem_base;
	parlcd_mem_base = map_phys_address(PARLCD_REG_BASE_PHYS, PARLCD_REG_SIZE, 0);
	if(parlcd_mem_base == NULL){
		printf("parlcd base failed, end program\n");
		exit(2);
	}
	parlcd_hx8357_init(parlcd_mem_base);
	parlcd_write_cmd(parlcd_mem_base, 0x2c);
	bool* lcd_array =(bool*) malloc(sizeof(bool) * LCD_W * LCD_H);
	if(lcd_array == NULL){
		printf("malloc failed, end program\n");
		exit(1);
	}
	char text[LINES][LEN_STR];
	while(!quit){
		pthread_mutex_lock(&mtx);
		sprintf(text[0], "%d/%d", data->lcd_rotation, data->rotation_user);
		sprintf(text[1], "PWM: %d", data->pwm);
		printf("\rrotation:%s %s ", text[0], text[1]);

		for(int y = 0; y < LINES; y++){
			for(int x = 0; x < strlen(text[y]); x++)
				zoom_let(x, y, text[y][x], SIZE_LET, lcd_array);
		}
		write_lcd(parlcd_mem_base, lcd_array);
		clear_array(lcd_array);
		fflush(stdout);
		if(!data->quit) pthread_cond_wait(&conv, &mtx);
		pthread_mutex_unlock(&mtx);
		quit = is_quit(data);
	}
	clear_lcd(parlcd_mem_base);
	printf("display_th end\n");
	free(lcd_array);
	return &ret;
}

void* period_th(void* d){
	data_t *data = (data_t*) d;
	printf("period_th start\n");
	bool quit = false;
	static int ret = 0;
	int counter = 0;
	bool counter_help = true;
	while(!quit){
		uint16_t c = *(volatile uint32_t*)(data->motor_base + DCSPDRV_REG_SR_o) & TENTH_BIT;
	    	if(c && counter_help){
			counter++;
	        	counter_help = false;
	        	if((counter % IRQ_PUL) == 0){
				counter = 0;
				pthread_mutex_lock(&mtx);
				data->rotation_real++;
				pthread_mutex_unlock(&mtx);
	        	}
		}
		else if( !(c && !counter_help) ) counter_help = true;
		quit = is_quit(data);
	}
	printf("period_th end\n");
	return &ret;
}
