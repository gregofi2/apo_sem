#include "main.h"

void* motor_th(void*d){
	printf("motor_th start\n");
	data_t *data = (data_t*) d;
	bool quit = false;
	static int ret  = 0;
	while(!quit){
		//read data from mutex controled struct
	    	pthread_mutex_lock(&mtx);
	    	int pwm = data->pwm;
	    	int rotation_real = data->rotation_real;
	    	int rotation_user = data->rotation_user;
	    	data->lcd_rotation = data->rotation_real;
	    	int leds = 0;
	    	if(pwm != 0) leds =  pwm / (MAX_PWM/MAX_LEDS);
	    	else leds = 0;
	    	data->leds_counter = set_led_line(data->leds_base, leds);
	    	pthread_mutex_unlock(&mtx);

	    	//control if user end rotation
	    	if(rotation_real == 0 && rotation_user == 0) pwm = 0;
	    	if(pwm == 0){
			RGB_color(data->leds_base, 'g', BOTH_LEDS);
	        	// nothing to regulate
	    	}
	    	else{
	        	//regulator
	        	if(rotation_user > rotation_real) pwm = pwm + PWM_STEP;
	        	else if(rotation_user < rotation_real) pwm = pwm - PWM_STEP;
	        	else{} //regulation end
	    		*(volatile uint32_t*)(data->motor_base + DCSPDRV_REG_CR_o) = 0;
            		*(volatile uint32_t*)(data->motor_base + DCSPDRV_REG_PERIOD_o) = 0;
            		*(volatile uint32_t*)(data->motor_base + DCSPDRV_REG_PERIOD_o) = 5000;
            		*(volatile uint32_t*)(data->motor_base + DCSPDRV_REG_CR_o) = DCSPDRV_REG_CR_PWM_ENABLE_m;
            		*(volatile uint32_t*)(data->motor_base + DCSPDRV_REG_DUTY_o) =  pwm * 5 | data->dir;
		}
		//write data from mutex controled struct
        	pthread_mutex_lock(&mtx);
	    	data->pwm = pwm;
	    	data->rotation_real = 0;
		pthread_cond_broadcast(&conv);
	    	pthread_mutex_unlock(&mtx);
	    	//brotcast display
	    	sleep(DELAY_S);
	    	quit = is_quit(data);
	}
	printf("motor_th end\n");
	return &ret;
}

