#ifndef KEYBOARD_H
#define KEYBOARD_H

#include "main.h"

void set_rotation(data_t* data);

void call_termios(int reset);

void set_dir(data_t* data);

void init_rotation(data_t* data);

#endif
